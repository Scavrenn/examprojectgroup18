class Customer(object):
    def __init__(self, customer_id, budget, type):
        self.customer_id = customer_id
        self.budget = budget
        self.type = type
        self.history = []

    def add_to_history(self, purchase):
        self.history.append(purchase)

    def pay(self, price):
        self.budget -= price

    def description(self):
        return 'Customer: ' + self.customer_id + ' of type: ' + self.type

    def summary(self):
        desc = ' bought: \n'
        for order in self.history:
            desc += '  YEAR: ' + str(order.year) + ' DAY: ' + str(order.day_of_year) + ' SLOT: ' + order.slot + '\n'
            desc += '  - ' + order.drink.name + ' for ' + str(order.drink.price) + '\n'
            desc += '  - ' + order.food.name + ' for ' + str(order.food.price) + '\n'
        return 'Customer: ' + self.customer_id + ' with remaining budget of ' + str(self.budget) + desc


class Order(object):
    def __init__(self, year, day_of_year, slot, drink, food, tip):
        self.year = year
        self.day_of_year = day_of_year
        self.slot = slot
        self.food = food
        self.drink = drink
        self.tip = tip

    def calculate_amount(self):
        return self.food.price + self.drink.price + self.tip


class Product(object):
    def __init__(self, name, price):
        self.name = name
        self.price = price

