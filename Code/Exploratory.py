import os
import pandas as pd
import matplotlib.pyplot as plt


def build_plots(df):
    dff = df[['CUSTOMER', 'FOOD']]
    ax = dff.groupby(['FOOD']).count().plot(kind='bar', title="Total amount of sold foods", legend=False, fontsize=12)
    ax.set_xlabel("Food", fontsize=12)
    ax.set_ylabel("Quantity", fontsize=12)
    plt.show()

    dff = df[['CUSTOMER', 'DRINKS']]
    ax = dff.groupby(['DRINKS']).count().plot(kind='bar', title="Total amount of sold drinks", legend=False, fontsize=12)
    ax.set_xlabel("Drink", fontsize=12)
    ax.set_ylabel("Quantity", fontsize=12)
    plt.show()


def build_probability_dict(df, row_name):
    dictionary = {}

    # Groups by timeslot and calculates probability (product/total_products)
    group = df.groupby([df['SLOT'], df[row_name]]).agg({row_name: 'count'})
    probability_df = group.groupby(level=['SLOT']).apply(lambda x: x / float(x.sum()))

    print(probability_df)

    # Converts dataframe to use it as dict
    for index, row in probability_df.iterrows():
        # print(index)
        # print(row)
        slot = index[0]
        product = index[1]
        probability = row[row_name]

        if slot not in dictionary:
            dictionary[slot] = {}
        dictionary[slot][product] = probability
    # print(dictionary)
    return dictionary


def format_data_frame(df):
    # Splits time column into date (YYYY-MM-DD) and slot (HH-MM-SS)
    df['DATE'], df['SLOT'] = df['TIME'].str.split(' ', 1).str

    # NaN values are excluded from grouby so use placeholder
    df['FOOD'] = df['FOOD'].fillna("nothing")

    return df


def print_info(df):
    # Prints drinks sold by the coffee bar
    print(df['DRINKS'].unique())

    # Prints food sold by the coffee bar
    print(df['FOOD'].unique())

    # Prints number of unique customers
    print(df['CUSTOMER'].nunique())


def explore():
    drinks_dict = {}
    food_dict = {}
    in_path = os.path.abspath("../Data/Coffeebar.csv")

    if not os.path.exists(in_path):
        print("Input file does not exist!")
    else:
        # Reads .csv file
        df = pd.read_csv(in_path, sep=";")

        # Formats data frame
        df = format_data_frame(df)

        # Prints info
        print_info(df)

        # Build plots
        build_plots(df)

        # Builds drinks dictionary
        drinks_dict = build_probability_dict(df, 'DRINKS')

        # Builds food dictionary
        food_dict = build_probability_dict(df, 'FOOD')

    return drinks_dict, food_dict
#return dictionnary by using calling program


if __name__ == '__main__':
    explore()
