import csv
import random
from models import Customer, Order, Product
from Exploratory import explore


# Constants

NUMBER_OF_YEARS = 5
DAYS_IN_YEAR = 365

BUDGET_ONE_TIME = 100
BUDGET_REGULAR = 250
BUDGET_HIPSTER = 500

NUMBER_RETURNING_CUSTOMERS = 1000

DRINK_PRICES = {'frappucino': 4, 'soda': 3, 'coffee': 3, 'tea': 3, 'water': 2, 'milkshake': 5}
FOOD_PRICES = {'sandwich': 5, 'pie': 3, 'muffin': 3, 'cookie': 2}


# Functions

def customer_id_exist(customer_id):
    return customer_id in customer_ids_used


def generate_random():
    return random.uniform(0, 1)


def generate_cid():
    number = str(int(random.uniform(0, 99999999)))
    while len(number) < 9:
        number = '0' + number
    return 'CID' + number


def generate_customer_id():
    customer_id = generate_cid()
    while customer_id_exist(customer_id):
        customer_id = generate_cid()
    customer_ids_used.add(customer_id)
    return customer_id


def generate_tip():
    return round(random.uniform(1, 10), 2)


def get_random_returning_customers_index(returning_customers_pickable):
    return int(random.uniform(0, (len(returning_customers_pickable))))


def get_product(probability, dic):
    product = ''
    prob_sum = 0
    for k, v in dic.items():
        prob_sum += v
        if probability < prob_sum:
            product = k
            break
    return product


def can_pay_max_order(budget):
    max_drink = 0
    for k, v in DRINK_PRICES.items():
        if max_drink < v:
            max_drink = v

    max_food = 0
    for k, v in FOOD_PRICES.items():
        if max_food < v:
            max_food = v

    return budget >= (max_drink + max_food)


def pick_a_random_customer(returning_customers_pickable):
    # Generate random to define one-time or returnee
    rand = generate_random()
    if rand <= 0.8:

        # Random to define tripAdvisor or regular
        rand_one_time_customer = generate_random()
        if rand_one_time_customer <= (1 / 10):
            # TripAdvisor
            return Customer(generate_customer_id(), BUDGET_ONE_TIME, 'TRIP_ADVISOR')
        else:
            return Customer(generate_customer_id(), BUDGET_ONE_TIME, 'ONE_TIME_REGULAR')

    else:
        if len(returning_customers_pickable) > 0:
            index = get_random_returning_customers_index(returning_customers_pickable)
            return returning_customers_pickable[index]
        else:
            return None


def create_an_order(customer, year, day, slot, drinks_probabilities, food_probabilities):
    # Randomly find a drink
    rand_drink = generate_random()
    drink_name = get_product(rand_drink, drinks_probabilities)
    drink_price = DRINK_PRICES[drink_name]
    drink_ordered = Product(drink_name, drink_price)

    # Randomly find a food
    rand_food = generate_random()
    food_name = get_product(rand_food, food_probabilities)
    food_price = 0
    if not food_name == '' and not food_name == 'nothing':
        food_price = FOOD_PRICES[food_name]
    food_ordered = Product(food_name, food_price)

    tip = 0

    # Generate tip if customer is TripAdvisor
    if customer.type == 'TRIP_ADVISOR':
        tip = generate_tip()

    return Order(year, day, slot, drink_ordered, food_ordered, tip)


def generate_returning_customers():
    returning_customers = []

    number_of_hipsters = int(NUMBER_RETURNING_CUSTOMERS / 3)
    number_of_regular = NUMBER_RETURNING_CUSTOMERS - number_of_hipsters

    for i in range(0, number_of_hipsters):
        customer = Customer(generate_customer_id(), BUDGET_HIPSTER, 'HIPSTER')
        returning_customers.append(customer)

    for i in range(0, number_of_regular):
        customer = Customer(generate_customer_id(), BUDGET_REGULAR, 'RETURNING_REGULAR')
        returning_customers.append(customer)

    return returning_customers


def write_output(results):
    print("Start writing results...")
    with open('../Results/results.csv', 'w', newline='') as file:
        spamwriter = csv.writer(file, delimiter=';')
        spamwriter.writerow(['YEAR', 'DAY_OF_YEAR', 'SLOT', 'CUSTOMERID', 'DRINK', 'DRINK_PRICE', 'FOOD', 'FOOD_PRICE', 'TIP'])
        for result in results:
            spamwriter.writerow(
                [result['year'],
                 result['day_of_year'],
                 result['slot'],
                 result['customer_id'],
                 result['drink'],
                 result['drink_price'],
                 result['food'],
                 result['food_price'],
                 result['tip']])


# Main

def main():
    print("Start processing...")
    results = []

    # Gets probabilities from Exploratory
    drinks_dict, food_dict = explore()
    # Defines slots based on Exploratory results [8:00, 8:05, ...]
    slots = drinks_dict.keys()
    # Generate returning customers
    returning_customers = generate_returning_customers()
    # Initialize list of the returning customers that can be picked (based on budget)
    returning_customers_pickable = [elem for elem in returning_customers]

    # DEBUG
    # print(drinks_dict)
    # print(food_dict)
    # print(slots)
    # for v in returning_customers:
        # print(v.description())

    # Loop through the years
    for year in range(0, NUMBER_OF_YEARS):

        # Loop through the days in one year
        for day in range(0, DAYS_IN_YEAR):

            # Loop through the time slots
            for slot in slots:

                # Pick a random customer
                customer = pick_a_random_customer(returning_customers_pickable)
                # print(customer.description())
                if customer is not None:
                    # Create an order
                    order = create_an_order(customer, (year + 1), (day + 1), slot, drinks_dict[slot], food_dict[slot])
                    # Update customer's budget
                    amount = order.calculate_amount()
                    customer.pay(amount)

                    # If returning customer, add order to history
                    if customer.type == 'HIPSTER' or customer.type == 'RETURNING_REGULAR':
                        for v in returning_customers:
                            if v.customer_id == customer.customer_id:
                                v.add_to_history(order)

                        # If budget too low, remove customer from the pickable list
                        if not can_pay_max_order(customer.budget):
                            returning_customers_pickable.remove(customer)
                            if len(returning_customers_pickable) == 0:
                                print('No more returning customer can pay an order!')

                    results.append({'year': order.year,
                                    'day_of_year': order.day_of_year,
                                    'slot': order.slot,
                                    'customer_id': customer.customer_id,
                                    'drink': order.drink.name,
                                    'drink_price': order.drink.price,
                                    'food': order.food.name,
                                    'food_price': order.food.price,
                                    'tip': order.tip})

    write_output(results)

    for cust in returning_customers:
        print(cust.summary())

    import pandas as pd
    import matplotlib.pyplot as plt
    df = pd.read_csv('../Results/results.csv', sep=";")
    dff = df[['CUSTOMERID', 'FOOD']]
    ax = dff.groupby(['FOOD']).count().plot(kind='bar', title="Total amount of sold foods", legend=False, fontsize=12)
    ax.set_xlabel("Food", fontsize=12)
    ax.set_ylabel("Quantity", fontsize=12)
    plt.show()

    # df['AMOUNT'] = df['DRINK_PRICE'] + df['FOOD_PRICE'] + df['TIP']
    # print(df.groupby(['YEAR', 'DAY_OF_YEAR']).sum())


if __name__ == '__main__':
    # List of id's already used
    customer_ids_used = set()

    main()